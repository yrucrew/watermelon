package com.ostrich.watermelon.user.biz.convert;

import com.ostrich.watermelon.user.api.bo.AdminBO;
import com.ostrich.watermelon.user.biz.model.admin.Admin;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AdminBOConvert {

    AdminBOConvert INSTANCE = Mappers.getMapper(AdminBOConvert.class);

    @Mappings({})
    AdminBO d2b(Admin admin);

}
