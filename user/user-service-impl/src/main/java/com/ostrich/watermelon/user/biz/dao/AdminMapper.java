package com.ostrich.watermelon.user.biz.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ostrich.watermelon.user.biz.model.admin.Admin;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AdminMapper extends BaseMapper<Admin> {

    @Select("select * from admins where account = #{account}")
    Admin selectByAccount(@Param("account") String acc);

    @Select("select count(1) from admins where status = 1")
    int countAdmins();

    @Select("select * from admins where status = 1 limit #{offset}, #{limit}")
    List<Admin> selectAdmins(@Param("offset") int offset, @Param("limit") int limit);

    @Update("update admins set status = 0 where id = #{uid}")
    void deleteAdmin(@Param("uid") Long uid);
}
