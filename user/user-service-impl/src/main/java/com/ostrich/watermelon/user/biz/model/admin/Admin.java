package com.ostrich.watermelon.user.biz.model.admin;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
* <pre>
* 这是自动生产的代码，不要乱改
*
* 管理员账号
* </pre>
*/
@Data
@TableName(value = "admins")
public class Admin {
      /**  */
      @TableId(value = "id" , type = com.baomidou.mybatisplus.annotation.IdType.AUTO)
      private Long id;

      /** 账号 */
      @TableField(value = "account")
      private String account;

      @TableField(value = "name")
      private String name;

      /** email */
      @TableField(value = "email")
      private String email;

      /**  */
      @TableField(value = "password")
      private String password;

      @TableField(value = "role_id")
      private String roleId;

      @TableField(value = "status")
      private Integer status;

      /**  */
      @TableField(value = "created_at")
      private java.util.Date createdAt;

      /**  */
      @TableField(value = "updated_at")
      private java.util.Date updatedAt;

      /** 联系人 */
      @TableField(value = "contact")
      private String contact;


}
