package com.ostrich.watermelon.user.biz.cache.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ostrich.watermelon.user.api.bo.AdminBO;
import com.ostrich.watermelon.user.biz.cache.AdminCache;
import com.ostrich.watermelon.user.biz.convert.AdminBOConvert;
import com.ostrich.watermelon.user.biz.dao.AdminMapper;
import com.ostrich.watermelon.user.biz.model.admin.Admin;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames = "Admin")
public class AdminCacheImpl extends ServiceImpl<AdminMapper, Admin> implements AdminCache {

    @Override
    @Cacheable(value = "Admin", key = "'selectByAccount_'+#account")
    public AdminBO selectByAccount(String account) {
        return AdminBOConvert.INSTANCE.d2b(baseMapper.selectByAccount(account));
    }
}
