package com.ostrich.watermelon.user.biz.cache;

import com.ostrich.watermelon.user.api.bo.AdminBO;

public interface AdminCache {

    AdminBO selectByAccount(String account);
}
