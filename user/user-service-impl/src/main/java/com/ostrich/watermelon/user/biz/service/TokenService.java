package com.ostrich.watermelon.user.biz.service;

import com.ostrich.common.framework.constant.RedisKey;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.xml.bind.DatatypeConverter;
import java.nio.ByteBuffer;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class TokenService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Value("${auth.token.namespace}")
    String tokenNamespace;

    public Long getUid(String token) {
        if (StringUtils.isEmpty(token) || token.length() != 32) {
            return null;
        }
        Long uid = resolveToken(token);
        String cachedToken = (String) this.redisTemplate.opsForValue().get(this.redisKey(uid));
        if (!token.equals(cachedToken)) {
            log.warn("[getUid] token not matched, uid:{}, paramToken:{}, cachedToken:{}", uid, token, cachedToken);
            return null;
        }

        return uid;
    }

    public String newToken(long uid, long expireSeconds) {
        Random rand = ThreadLocalRandom.current();
        int rand1 = rand.nextInt();
        int rand2 = rand.nextInt();
        int high32 = (int) (uid >> 32);
        int low32 = (int) uid;

        ByteBuffer buf = ByteBuffer.allocate(16);
        buf.putInt(rand1);
        buf.putInt(rand2);
        buf.putInt((rand1 & rand2) ^ high32);
        buf.putInt((rand1 | rand2) ^ low32);

        String token = DatatypeConverter.printHexBinary(buf.array());
        this.redisTemplate.opsForValue().set(this.redisKey(uid), token, expireSeconds, TimeUnit.SECONDS);
        return token;
    }

    public void keepToken(long uid, int duration, TimeUnit unit) {
        this.redisTemplate.expire(this.redisKey(uid), duration, unit);
    }

    public void removeToken(long uid) {
        this.redisTemplate.delete(this.redisKey(uid));
    }

    private String redisKey(long uid) {
        return this.tokenNamespace + "/" + RedisKey.loginUserTokenPrefix.key(uid);
    }

    public static void main(String[] args) {
        String token = "9705F72F8D7F4EA2850546229F7FD8B8";
        System.out.println("uid:" + resolveToken(token));
    }

    private static Long resolveToken(String token) {
        byte[] bytes = DatatypeConverter.parseHexBinary(token);
        ByteBuffer buf = ByteBuffer.wrap(bytes);
        int rand1 = buf.getInt();
        int rand2 = buf.getInt();
        int randHigh32 = buf.getInt();
        int randLow32 = buf.getInt();

        long high32 = (rand1 & rand2) ^ randHigh32;
        int low32 = (rand1 | rand2) ^ randLow32;
        return (high32 << 32) | (low32 & 0xFFFFFFFFL);
    }
}
