package com.ostrich.watermelon.user.biz.service;

import com.ostrich.common.framework.constant.RedisConstant;
import com.ostrich.common.framework.constant.StatusConsts;
import com.ostrich.common.framework.util.PasswordUtils;
import com.ostrich.common.framework.pojo.Response;
import com.ostrich.watermelon.user.api.AdminService;
import com.ostrich.watermelon.user.api.bo.AdminBO;
import com.ostrich.watermelon.user.biz.cache.AdminCache;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Service
@Slf4j
public class AdminServiceImpl implements AdminService {

    @Autowired
    TokenService tokenService;

    @Autowired
    AdminCache adminCache;

    @Override
    public Response<AdminBO> login(Map<String, String> data) {
        String account = data.getOrDefault("account", "");
        String password = data.getOrDefault("password", "");
        AdminBO adminBO = selectByAccount(account);
        if (adminBO == null) {
            log.error("[login] account not exists, acc:{}", account);
            return Response.err(403, "用户名或密码不正确");
        }

        if (adminBO.getStatus() != StatusConsts.EnableStatus.ENABLED) {
            log.error("[login] account blocked, account:{}", account);
            return Response.err(403, "用户名或密码不正确");
        }

        if (!PasswordUtils.validatePassword(password, adminBO.getPassword())) {
            log.error("[login] password not match, acc:{}", account);
            return Response.err(403, "用户名或密码不正确");
        }
        String token = tokenService.newToken(adminBO.getId(), RedisConstant.TOKEN_EXPIRE_SECONDS);
        return Response.data(AdminBO.build(adminBO, token));
    }

    @Override
    public AdminBO selectByAccount(String account) {
        return adminCache.selectByAccount(account);
    }
}
