package com.ostrich.watermelon.user.api;

import com.ostrich.common.framework.pojo.Response;
import com.ostrich.watermelon.user.api.bo.AdminBO;

import java.util.Map;

public interface AdminService {

    Response<AdminBO> login(Map<String, String> data);

    AdminBO selectByAccount(String account);

}
