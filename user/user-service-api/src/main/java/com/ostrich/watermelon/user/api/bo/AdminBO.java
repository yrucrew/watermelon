package com.ostrich.watermelon.user.api.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * <pre>
 * 这是自动生产的代码，不要乱改
 *
 * 管理员账号
 * </pre>
 */
@Data
public class AdminBO implements Serializable {
    private Long id;
    private String account;
    private String username;
    private String contact;
    private String password;
    private Integer status;
    private String token;


    public static AdminBO build(AdminBO adminBO, String token) {
        AdminBO vo = new AdminBO();
        vo.id = adminBO.getId();
        vo.account = adminBO.getAccount();
        vo.setUsername(adminBO.getUsername());
        vo.setContact(adminBO.getContact());
        vo.setPassword(adminBO.getPassword());
        vo.token = token;
        return vo;
    }
}
