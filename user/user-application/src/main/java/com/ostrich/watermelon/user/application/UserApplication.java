package com.ostrich.watermelon.user.application;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableCaching
@EnableAsync(proxyTargetClass = true)
@EnableDubbo(scanBasePackages = "com.ostrich.watermelon.user.biz.service")
@MapperScan({"com.ostrich.watermelon.user.biz.dao"})
@SpringBootApplication(scanBasePackages = {"com.ostrich.watermelon.user"})
public class UserApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }

}
