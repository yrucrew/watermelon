package com.ostrich.watermelon.user.application.controller;

import com.ostrich.common.framework.pojo.Response;
import com.ostrich.watermelon.user.api.AdminService;
import com.ostrich.watermelon.user.api.bo.AdminBO;
import com.ostrich.watermelon.user.application.convert.AdminVOConvert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Slf4j
@RequestMapping("/admin")
@RestController()
@Api(tags = "管理员")
public class AdminController {

    @Reference
    AdminService adminService;

    @PostMapping("/login")
    @ApiOperation(value = "登录", notes = "{\"account\":\"ostrich\",\"password\":\"123456\"}")
    public Response login(@RequestBody Map<String, String> data) {
        Response<AdminBO> response = adminService.login(data);
        return response.hasErr() ? response : Response.data(AdminVOConvert.INSTANCE.b2v(response.data));
    }

}
