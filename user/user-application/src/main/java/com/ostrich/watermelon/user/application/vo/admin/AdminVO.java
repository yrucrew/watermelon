package com.ostrich.watermelon.user.application.vo.admin;

import lombok.Data;

@Data
public class AdminVO {
    private Long id;
    private String account;
    private String username;
    private String contact;
    private String password;
    private String token;


}
