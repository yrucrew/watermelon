package com.ostrich.watermelon.user.application.convert;


import com.ostrich.watermelon.user.api.bo.AdminBO;
import com.ostrich.watermelon.user.application.vo.admin.AdminVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AdminVOConvert {

    AdminVOConvert INSTANCE = Mappers.getMapper(AdminVOConvert.class);

    @Mappings({})
    AdminVO b2v(AdminBO result);
}
