package com.ostrich.common.framework.json;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONToken;
import com.alibaba.fastjson.parser.deserializer.AbstractDateDeserializer;

import java.lang.reflect.Type;

public class SqlTimeDeserializer extends AbstractDateDeserializer {
    @Override
    protected <T> T cast(DefaultJSONParser parser, Type clazz, Object fieldName, Object val) {
        if (val == null) {
            return null;
        }

        if (val instanceof java.util.Date) {
            return (T) new java.sql.Time(((java.util.Date) val).getTime());
        }

        throw new IllegalArgumentException("unknown type:" + val.getClass().getName());
    }

    @Override
    public int getFastMatchToken() {
        return JSONToken.LITERAL_STRING;
    }
}
