package com.ostrich.common.framework.constant;

public interface StatusConsts {
    /**冻结、激活、可用状态*/
    interface EnableStatus {
        /**禁用*/
        int DISABLED = 0;
        /**可用*/
        int ENABLED = 1;
    }

    /**订单状态*/
    enum OrderStatus {
        /**等待支付*/
        CREATED(1, "待支付"),
        /**取消*/
        CANCEL(2, "已取消"),
        /**成功支付，等待物流*/
        PAID(3, "已支付"),
        /** 物流送达，等待提货 */
        WAITPICKUP(4, "等待提货"),
        /** 已提货 */
        PICKEDUP(5, "已提货"),
        /** 退款中 */
        REFUNDING(6, "退款中"),
        /** 已退款 */
        REFUNDED(7, "已退款"),
        ;

        private int code;
        private String name;
        OrderStatus(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return this.code;
        }

        public String getName() {
            return this.name;
        }

        public static OrderStatus codeOf(int code) {
            for (OrderStatus s : OrderStatus.values()) {
                if (s.code == code) {
                    return s;
                }
            }

            throw new RuntimeException("invalid order status:" + code);
        }
    }

    /** 退款状态 */
    enum RefundStatus {
        UNDO(0, "未处理"),
        AGREE(1, "同意退款"),
        REJECT(2, "拒绝退款"),
        ;
        private int code;
        private String name;
        RefundStatus(int code, String name) {
            this.code = code;
            this.name = name;
        }
        public int getCode() {
            return this.code;
        }
        public String getName() {
            return this.name;
        }
        public static OrderStatus codeOf(int code) {
            for (OrderStatus s : OrderStatus.values()) {
                if (s.code == code) {
                    return s;
                }
            }
            throw new RuntimeException("invalid order status:" + code);
        }
    }

    /***
     * 子单状态
     */
    interface ChildOrderStatus {
        /** 创建 */
        int CREATED = 1;
        /** 等待物流 */
        int WAIT_LOGISTICS = 2;
        /** 物流送达，等待提货 */
        int WAIT_PICK_UP = 3;
        /** 已提货 */
        int PICKED_UP = 4;
    }

    /***
     * 店员角色
     */
    enum StaffRole {
        /**普通员工*/
        general(1),
        /**店长*/
        manager(2);

        private int code;
        StaffRole(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }

    /**
     * 员工申请状态
     */
    interface StaffApplicationStatus {
        /** 0 - 拒绝*/
        int REJECT = 0;
        /** 1 - 申请中*/
        int APPLYING = 1;
        /** 2 - 申请通过*/
        int PASSED = 2;
    }

    /**
     * 门店入驻申请
     */
    enum  StoreApplicationStatus {
        APPLYING(0, "申请中"),
        ACCEPT(1, "已确认"),
        IGNORE(2, "已忽略");

        private int code;
        private String name;
        StoreApplicationStatus(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

        public static StoreApplicationStatus codeOf(int code) {
            for (StoreApplicationStatus s : StoreApplicationStatus.values()) {
                if (s.code == code) {
                    return s;
                }
            }

            throw new RuntimeException("invalid order status:" + code);
        }
    }

    /**
     * 商品状态
     */
    interface ProductStatus {
        /** 0 - 已删除 */
        int DELETED = 0;
        /** 1 - 上架 */
        int ON_SHOW = 1;
        /** 2 - 下架 */
        int OFF_SHOW = 2;
    }

    /***
     * 是否上架
     */
    enum proStatus {
        /**上架*/
        shangjia(1),
        /**下架*/
        xiajia(2);

        private int code;
        proStatus(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }

    /***
     * 商品图片类型
     */
    interface ProductImageType {
        /** 2 - 轮播图*/
        int SLIDES = 2;
        /** 1 - 详情图 */
        int DETAIL = 1;
    }

    /**
     * 提现状态
     */
    interface WithDrawStatus {
        /**
         * 待处理
         */
        int UNDO = 0;
        /**
         * 已打款
         */
        int SUCCESS = 1;
    }

    /**
     * 短信或订单的状态
     */
    interface WithSendMessageStatus {
        /**
         * 待发送或未打印
         */
        Long UNDO = 0l;
        /**
         * 已发送或已打印
         */
        Long SUCCESS = 1l;
    }

    interface SharePictureStatus {

        /**
         * 正常
         */
        long normal = 1;
        /**
         * 删除
         */
        long del = 0;
        /**
         * 选中
         */
        long pick = 2;
    }
}
