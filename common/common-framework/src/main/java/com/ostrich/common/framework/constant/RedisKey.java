package com.ostrich.common.framework.constant;

import java.util.StringJoiner;

/**
 * @author tanyijian on 2018-12-28
 */
public enum RedisKey {
    /** key: += appId */
    accessTokenPrefix("/wechat/app/accessToken/"),

    /**key: += uid*/
    userInfoPrefix("/userVo/"),

    /**用户token， key += uid*/
    loginUserTokenPrefix("/users/login/token/"),

    /**
     * 员工申请短信验证码
     * key += uid, mobile
     * val: 验证码
     */
    staffApplyVerifyCodePrefix("/staff/verify/code/"),

    /***
     * 短信lock
     * key += uid
     */
    smsLock("/sms/lock/"),

    /***
     * 绑定手机号码短信验证码key
     * key += phone
     */
    bindPhoneNumPrefix("/users/bind/phone"),

    /***
     * redis 分布式锁key
     * key += goodsId
     */
    redisLockGoodsPrefix("/redis/lock/goods"),

    /***
     * 微信公众号 + access_token
     */
    weChatSubscriptionAccessToken("/weChatSubscription");
    ;

    private String prefix;
    RedisKey(String prefix) {
        this.prefix = prefix;
    }

    public String key(Object... args) {
        StringJoiner joiner = new StringJoiner("/", this.prefix, "");
        for (Object arg : args) {
            joiner.add(arg.toString());
        }

        return joiner.toString();
    }
}
