package com.ostrich.common.framework.constant;

/**
 * Redis常量
 */
public class RedisConstant {

    public static final long TOKEN_EXPIRE_SECONDS = 60 * 60 * 24 * 3;

    public static final long USER_REDIS_CACHE_DAYS = 3;

}
