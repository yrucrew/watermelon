package com.ostrich.common.framework.pojo;

import java.io.Serializable;

public class Response<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String OK_MSG = "ok";
    public static final int OK_CODE = 200;
    public static final Response OK = new Response();

    public int code;
    public String msg;
    public T data;

    public static Response err(int code, String msg) {
        return new Response(code, msg);
    }

    public static <T> Response<T> err(int code, String msg, T t) {
        return new Response(code, msg,t);
    }

    public static <T> Response<T> toErr(Response resp) {
        return new Response(resp.code, resp.msg);
    }

    public static <T> Response<T> data(T data) {
        return new Response(data);
    }

    public static <T> Response<T> build(int code, String msg, T data) {
        Response r = new Response(code, msg);
        r.data = data;
        return r;
    }

    public Response() {
        this.code = OK_CODE;
        this.msg = OK_MSG;
    }

    public Response(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public Response(int code, String msg, T t){
        this.code = code;
        this.msg = msg;
        this.data = t;
    }


    public Response(T data){
        this();
        this.data = data;
    }

    public Response status(int code, String msg){
        this.code = code;
        this.msg = msg;
        return this;
    }

    /**
     * 判断响应是否成功
     * @return
     */
    public boolean hasErr(){
        return this.code != OK_CODE;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}

