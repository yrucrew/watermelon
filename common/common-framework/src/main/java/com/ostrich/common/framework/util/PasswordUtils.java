package com.ostrich.common.framework.util;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.ArrayUtils;

import javax.xml.bind.DatatypeConverter;
import java.security.SecureRandom;

public class PasswordUtils {
    public static final int SALT_SIZE = 8;

    private static byte[] generateSalt() {
        SecureRandom random = new SecureRandom();
        byte salt[] = new byte[SALT_SIZE];
        random.nextBytes(salt);
        return salt;
    }

    /**
     * @param plainPassword
     * @return hex(salt + md5(password+salt))
     */
    public static String encryptPassword(String plainPassword) {
        byte[] salt = generateSalt();
        byte[] pwdBytes = plainPassword.getBytes();
        return generateEncryptedPassword(salt, pwdBytes);
    }

    /**
     * 验证密码
     * @param plainPassword 明文密码
     * @param encryptedPassword 密文密码
     * @return 验证成功返回true
     */
    public static boolean validatePassword(String plainPassword, String encryptedPassword) {
        byte[] saltWithPwdMd5 = DatatypeConverter.parseHexBinary(encryptedPassword);
        byte[] salt = ArrayUtils.subarray(saltWithPwdMd5, 0, SALT_SIZE);

        String newPwd = generateEncryptedPassword(salt, plainPassword.getBytes());
        return newPwd.equals(encryptedPassword);
    }

    private static String generateEncryptedPassword(byte[] salt, byte[] pwdBytes) {
        byte[] hashPassword = DigestUtils.md5(ArrayUtils.addAll(pwdBytes, salt));
        return DatatypeConverter.printHexBinary(ArrayUtils.addAll(salt, hashPassword));
    }
}
