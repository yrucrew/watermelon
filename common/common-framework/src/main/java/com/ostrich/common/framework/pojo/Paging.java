package com.ostrich.common.framework.pojo;

import lombok.Data;

import java.util.List;

@Data
public class Paging<T> {
    private int total;
    private int pageNo;
    private int pageSize;
    List<T> records;

    public Paging(int total, int pageNo, int pageSize, List<T> records) {
        this.total = total;
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.records = records;
    }
}
