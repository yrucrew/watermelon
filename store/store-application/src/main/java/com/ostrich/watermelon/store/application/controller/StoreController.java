package com.ostrich.watermelon.store.application.controller;

import com.ostrich.common.framework.pojo.Paging;
import com.ostrich.common.framework.pojo.Response;
import com.ostrich.watermelon.store.api.StoreService;
import com.ostrich.watermelon.store.api.bo.StoreBO;
import com.ostrich.watermelon.store.application.convert.StoreVOConvert;
import com.ostrich.watermelon.store.application.vo.StoreVO;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RequestMapping("/stores")
@RestController()
@Api(tags = "门店")
public class StoreController {

    @Reference
    StoreService storeService;

    @GetMapping("")
    public Response<Paging<StoreVO>> getStores(@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                               @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        Paging<StoreBO> paging = storeService.getStorePage(pageNo, pageSize);
        if (!CollectionUtils.isEmpty(paging.getRecords())) {
            Paging<StoreVO> voPaging =
                    new Paging<>(
                            paging.getTotal(),
                            paging.getPageNo(),
                            paging.getPageSize(),
                            StoreVOConvert.INSTANCE.bos2Vos(paging.getRecords())
                    );
            return Response.data(voPaging);
        }
        return null;
    }
}
