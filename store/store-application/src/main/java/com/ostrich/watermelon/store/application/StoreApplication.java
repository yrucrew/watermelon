package com.ostrich.watermelon.store.application;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableCaching
@EnableAsync(proxyTargetClass = true)
@EnableDubbo(scanBasePackages = "com.ostrich.watermelon.store.biz.service")
@MapperScan({"com.ostrich.watermelon.store.biz.dao"})
@SpringBootApplication(scanBasePackages = {"com.ostrich.watermelon.store"})
public class StoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(StoreApplication.class, args);
    }

}
