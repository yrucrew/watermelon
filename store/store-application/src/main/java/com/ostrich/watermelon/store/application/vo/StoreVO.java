package com.ostrich.watermelon.store.application.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.ostrich.common.framework.json.SqlTimeDeserializer;
import lombok.Data;

@Data
public class StoreVO {

    private Long id;

    private String name;

    private String address;

    private String postcode;

    private String contact;

    private String phone;

    @JSONField(format="HH:mm", deserializeUsing = SqlTimeDeserializer.class)
    private java.sql.Time openAt;

    @JSONField(format="HH:mm", deserializeUsing = SqlTimeDeserializer.class)
    private java.sql.Time closeAt;

    /**
     * 打款银行
     */
    private String bankName;

    /**
     * 银行卡号
     */
    private String bankCard;


    /** 团购商品默认销售结束时间 */
    private java.sql.Time groupSaleEnd;

    /** 团购商品默认销售开始时间 */
    private java.sql.Time groupSaleBeg;

    /** 团购商品默认提货时间 */
    private java.sql.Time pickUpTime;

    /**经度*/
    private String longitude;

    /**纬度*/
    private String latitude;

    /**省*/
    private String province;

    /**市*/
    private String city;

    /**县区*/
    private String county;

    /**街道*/
    private String street;
}
