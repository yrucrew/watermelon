package com.ostrich.watermelon.store.application.convert;

import com.ostrich.watermelon.store.api.bo.StoreBO;
import com.ostrich.watermelon.store.application.vo.StoreVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface StoreVOConvert {

    StoreVOConvert INSTANCE = Mappers.getMapper(StoreVOConvert.class);

    List<StoreVO> bos2Vos(List<StoreBO> list);
}
