package com.ostrich.watermelon.store.api.bo;

import lombok.Data;

@Data
public class StoreBO {

    private Long id;

    /**
     * 门店名称
     */
    private String name;

    /**
     * 地址
     */
    private String address;

    /**
     * 邮编
     */
    private String postcode;

    /**
     * 联系人
     */
    private String contact;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 0-删除；1-正常;
     */
    private Integer status;

    /**
     * 营业开始时间
     */
    private java.sql.Time openAt;

    /**
     * 营业结束时间
     */
    private java.sql.Time closeAt;

    /**
     * 销售开始时间
     */
    private java.sql.Time saleBeg;

    /**
     * 销售结束时间
     */
    private java.sql.Time saleEnd;

    /**
     * 提货时间
     */
    private java.sql.Time pickUpTime;

    /**
     * 创建时间
     */
    private java.util.Date createdAt;

    /**
     * 更新时间
     */
    private java.util.Date updatedAt;

    /**
     * 打印机编码（根据电话号码绑定）
     */
    private String bindPrinterCode;

    /**
     * 团购商品默认销售开始时间
     */
    private java.sql.Time groupSaleBeg;

    /**
     * 团购商品默认销售结束时间
     */
    private java.sql.Time groupSaleEnd;

    /**
     * 万分比的比例 提现金额(默认是百分之10个点)
     */
    private Long withdrawalRate;

    /**
     * 打款银行
     */
    private String bankName;

    /**
     * 银行卡号
     */
    private String bankCard;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 县区
     */
    private String county;

    /**
     * 街道
     */
    private String street;
}
