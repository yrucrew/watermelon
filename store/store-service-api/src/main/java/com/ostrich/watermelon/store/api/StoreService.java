package com.ostrich.watermelon.store.api;

import com.ostrich.common.framework.pojo.Paging;
import com.ostrich.watermelon.store.api.bo.StoreBO;

public interface StoreService {

    Paging<StoreBO> getStorePage(Integer pageNo, Integer pageSize);

}
