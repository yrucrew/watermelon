package com.ostrich.watermelon.store.biz.convert;

import com.ostrich.watermelon.store.api.bo.StoreBO;
import com.ostrich.watermelon.store.biz.model.store.Store;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface StoreBOConvert {

    StoreBOConvert INSTANCE = Mappers.getMapper(StoreBOConvert.class);

    @Mappings({})
    StoreBO do2bo(Store store);

    @Mappings({})
    List<StoreBO> dos2Bos(List<Store> dos);
}
