package com.ostrich.watermelon.store.biz.service;

import com.ostrich.common.framework.constant.StatusConsts;
import com.ostrich.common.framework.pojo.Paging;
import com.ostrich.watermelon.store.api.StoreService;
import com.ostrich.watermelon.store.api.bo.StoreBO;
import com.ostrich.watermelon.store.biz.cache.StoreCache;
import com.ostrich.watermelon.store.biz.convert.StoreBOConvert;
import com.ostrich.watermelon.store.biz.model.store.Store;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Service
@Slf4j
public class StoreServiceImpl implements StoreService {

    @Autowired
    StoreCache storeCache;

    @Override
    public Paging<StoreBO> getStorePage(Integer pageNo, Integer pageSize) {
        int total = storeCache.countStoreByStatus(StatusConsts.EnableStatus.ENABLED);
        Integer offset = (pageNo - 1) * pageSize;
        List<Store> dos = storeCache.getStorePage(StatusConsts.EnableStatus.ENABLED, offset, pageSize);
        List<StoreBO> bos = StoreBOConvert.INSTANCE.dos2Bos(dos);
        return new Paging<>(total, pageNo, pageSize, bos);
    }
}
