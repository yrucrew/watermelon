package com.ostrich.watermelon.store.biz.model.store;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
* <pre>
* 这是自动生产的代码，不要乱改
*
* 门店信息
* </pre>
*/
@Data
@TableName(value = "stores")
public class Store implements Serializable {
      /**  */
      @TableId(value = "id" , type = IdType.AUTO)
      private Long id;

      /** 门店名称 */
      @TableField(value = "name")
      private String name;

      /** 地址 */
      @TableField(value = "address")
      private String address;

      /** 邮编 */
      @TableField(value = "postcode")
      private String postcode;

      /** 联系人 */
      @TableField(value = "contact")
      private String contact;

      /** 联系电话 */
      @TableField(value = "phone")
      private String phone;

      /** 0-删除；1-正常; */
      @TableField(value = "status")
      private Integer status;

      /** 营业开始时间 */
      @TableField(value = "open_at")
      private java.sql.Time openAt;

      /** 营业结束时间 */
      @TableField(value = "close_at")
      private java.sql.Time closeAt;

      /** 销售开始时间 */
      @TableField(value = "group_sale_beg")
      private java.sql.Time saleBeg;

      /** 销售结束时间 */
      @TableField(value = "group_sale_end")
      private java.sql.Time saleEnd;

      /** 提货时间 */
      @TableField(value = "pick_up_time")
      private java.sql.Time pickUpTime;

      /** 创建时间 */
      @TableField(value = "created_at")
      private java.util.Date createdAt;

      /** 更新时间 */
      @TableField(value = "updated_at")
      private java.util.Date updatedAt;

      /**
       * 打印机编码（根据电话号码绑定）
       */
      @TableField(value = "bind_printer_code")
      private String bindPrinterCode;

      /** 团购商品默认销售开始时间 */
      @TableField(value = "group_sale_beg")
      private java.sql.Time groupSaleBeg;

      /** 团购商品默认销售结束时间 */
      @TableField(value = "group_sale_end")
      private java.sql.Time groupSaleEnd;

      /** 万分比的比例 提现金额(默认是百分之10个点) */
      @TableField(value = "withdrawal_rate")
      private Long withdrawalRate;

      /**
       * 打款银行
       */
      @TableField(value = "bank_name")
      private String bankName;

      /**
       * 银行卡号
       */
      @TableField(value = "bank_card")
      private String bankCard;

      /**经度*/
      @TableField(value = "longitude")
      private String longitude;

      /**纬度*/
      @TableField(value = "latitude")
      private String latitude;

      /**省*/
      @TableField(value = "province")
      private String province;

      /**市*/
      @TableField(value = "city")
      private String city;

      /**县区*/
      @TableField(value = "county")
      private String county;

      /**街道*/
      @TableField(value = "street")
      private String street;
}
