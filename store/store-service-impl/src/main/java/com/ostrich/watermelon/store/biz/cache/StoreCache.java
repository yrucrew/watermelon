package com.ostrich.watermelon.store.biz.cache;

import com.ostrich.watermelon.store.biz.model.store.Store;

import java.util.List;

public interface StoreCache {

    int countStoreByStatus(int enabled);

    List<Store> getStorePage(int enabled, int offset, int pageSize);
}
