package com.ostrich.watermelon.store.biz.cache.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ostrich.watermelon.store.biz.cache.StoreCache;
import com.ostrich.watermelon.store.biz.dao.StoreMapper;
import com.ostrich.watermelon.store.biz.model.store.Store;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@CacheConfig(cacheNames = "Store")
public class StoreCacheImpl extends ServiceImpl<StoreMapper, Store> implements StoreCache {

    @Override
    @Cacheable(value = "Store", key = "'countStoreByStatus_'+ #enabled")
    public int countStoreByStatus(int enabled) {
        return baseMapper.countStoresByStatus(enabled);
    }

    @Override
    @Cacheable(value = "Store", key = "'getStorePage_'+ #enabled + '_' + #offset + '_' + #pageSize")
    public List<Store> getStorePage(int enabled, int offset, int pageSize) {
        return baseMapper.selectStoresByStatus(enabled, offset, pageSize);
    }
}
