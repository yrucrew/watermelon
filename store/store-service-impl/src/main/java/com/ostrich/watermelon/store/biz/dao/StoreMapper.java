package com.ostrich.watermelon.store.biz.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ostrich.watermelon.store.biz.model.store.Store;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public interface StoreMapper extends BaseMapper<Store> {

    @Select("select count(1) from stores where status = #{status}")
    int countStoresByStatus(@Param("status") int status);

    @Select("select * from stores where status = #{status} order by id desc limit #{offset}, #{limit}")
    List<Store> selectStoresByStatus(@Param("status") int status, @Param("offset") int offset, @Param("limit") int limit);

}
